-----------------------------------------------
Niels Schlusser, April, 22th 2024
-----------------------------------------------

This directory contains the python scripts for postprocessing after the EM procedure, including ranking of the extracted PWMs according to their relative KD and plotting a summary plot for run statistics as well as binding motifs.

