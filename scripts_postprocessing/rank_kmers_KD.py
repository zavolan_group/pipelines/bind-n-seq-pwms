'''
script searches output file for PWMs
subtracts the respective background probability
prints a ranked list of accordingly ranked PWMs
'''
import os
import sys
import pandas as pd
import numpy as np

#specify prefix for files
prefix = str(sys.argv[-1])

#define thresholds
spec_threshold = 0.0
sq_precision = 1e-3

reference_PWM_length = 5

#save original output channel
original_stdout = sys.stdout

#define nucleic acid dictionary
nucl_dict = {"A" : 0, "C" : 1, "G" : 2, "T" : 3 }
inv_nucl_dict = {0 : "A", 1 : "C", 2 : "G", 3 : "T" }



'''
define class of PWMs with score
'''
class PWM_Score():
	def __init__(self, PWM, fg_log_lh, diff_log_lh, iterations, E0):
		self.PWM = PWM
		self.fg_log_lh = fg_log_lh
		self.bg_log_lh = 0.0
		self.diff_log_lh = diff_log_lh
		self.iterations = iterations
		self.E0 = E0
		self.norm = 1.0
		self.len_corr_fact = 0.0
		self.log_KD_rel = 0.0




'''
method gives distance between two PWMs and E0s
'''
def distance(p1,p2):
	PWM1_length = len(p1.PWM)
	PWM2_length = len(p2.PWM)

	if PWM1_length == PWM2_length:
		d = 0.0
		for a,b in zip(p1.PWM,p2.PWM):
			d += np.power((a-b),2)

		d /= PWM1_length

		d += np.power((p1.E0 - p2.E0),2)

		d /= 2

		return d
	else:
		return np.inf



'''
method removes duplicates
'''
def remove_duplicates(pArray):
	deduplicated_PWM_array = []

	for p in pArray:
		isind = False

		for d in deduplicated_PWM_array:
			if distance(p,d) < sq_precision:
				isind = True

		if isind == False:
			deduplicated_PWM_array.append(p)

	return deduplicated_PWM_array



'''
Method calculates the baseline P(D|N...N) of a kmer of unit distribution of given length, as well as length correction factor and overall normalization.
Iterates over all files starting with "ENCFF" and ending with "mer.dat" in directory.
Assumes infinitely specific binding (E_0 to -infinity)

Args:
	length of PWM
	length of reference PWM

Returns:
	PD of the reference PWM
	correction factor for length difference
	overall library size normalization factor
'''
def calculate_PD_baseline(PWM_length,ref_PWM_length):
	ret_ref = 0.0
	ret_len = 0.0
	norm = 0.0

	for fi in os.listdir(os.getcwd()):
		filename = os.fsdecode(fi)

		if filename.startswith("ENCFF") and filename.endswith("mer.dat"):
			df = pd.read_csv(filename,sep=" ",usecols=[0,1,2],names=['len','nbr','freq'])

			df = df[(df['len'] >= max(PWM_length,ref_PWM_length)) & (df['freq'] > 0.0)]

			L_arr = df['len'].to_numpy()
			n_arr = df['nbr'].to_numpy()
			f_arr = df['freq'].to_numpy()

			t1 = sum( n_arr * np.log(f_arr * (L_arr - ref_PWM_length + 1)) )
			t2 = sum(n_arr) * np.log( sum(f_arr * (L_arr - ref_PWM_length + 1)))

			ret_ref += t1 - t2

			ret_len += sum( n_arr * np.log((L_arr - PWM_length + 1) / (L_arr - ref_PWM_length + 1)) )

			norm += sum(n_arr)

	return ret_ref,ret_len,norm



'''
function reads the PWMs from results file in directory
'''
def read_list_PWMs():
	PWM_score_array = []

	for fi in os.listdir(os.getcwd()):
		filename = os.fsdecode(fi)

		if ((not filename.startswith("ENCFF")) and filename.endswith("mer.dat") and filename.startswith(prefix)):

			foreground_file = filename

			#open files
			f = open(foreground_file,'r')

			while True:
				# Get next PWM from file
				line1 = f.readline()	#skip first line
				line1 = f.readline()
				line2 = f.readline()
				line3 = f.readline()
				line4 = f.readline()
				line5 = f.readline()

				# if two lines are empty
				# end of file is reached
				if not (line1):
					break

				#extract words from lines
				val1 = line1.split()
				val2 = line2.split()
				val3 = line3.split()
				val4 = line4.split()
				val5 = line5.split()

				#skip free line after each PWM
				line1 = f.readline()

				Lstart = float(val5[1])

				line1 = f.readline()	#skip first line
				line1 = f.readline()
				line2 = f.readline()
				line3 = f.readline()
				line4 = f.readline()
				line5 = f.readline()

				# if two lines are empty
				# end of file is reached
				if not (line1):
					break

				#extract words from lines
				val1 = line1.split()
				val2 = line2.split()
				val3 = line3.split()
				val4 = line4.split()
				val5 = line5.split()

				#skip free line after each PWM
				line1 = f.readline()

				Lstop = float(val5[1])

				if (Lstart < Lstop and float(val5[0]) < spec_threshold):
					Lw = len(val1) - 1

					pPWM = [0.0 for j in range(0,4*Lw)]

					for i in range(0,Lw):
						pPWM[0*Lw+i] = float(val1[i+1])
					for i in range(0,Lw):
						pPWM[1*Lw+i] = float(val2[i+1])
					for i in range(0,Lw):
						pPWM[2*Lw+i] = float(val3[i+1])
					for i in range(0,Lw):
						pPWM[3*Lw+i] = float(val4[i+1])

					tmp_obj = PWM_Score(PWM = pPWM, fg_log_lh = Lstop, diff_log_lh = Lstop - Lstart, iterations = int(val5[2]), E0 = float(val5[0]))

					PWM_score_array.append(tmp_obj)


			f.close()

	return PWM_score_array



'''
set relative logarithmic KDs
Args:
	list of convergent PWMs
'''
def set_rel_log_KDs(PWM_list):
	for p in PWM_list:
		p.bg_log_lh, p.len_corr_fact, p.norm = calculate_PD_baseline(len(p.PWM)//4,reference_PWM_length)
		p.log_KD_rel = (p.bg_log_lh - p.fg_log_lh + p.len_corr_fact) / p.norm
	return PWM_list



'''
prints the PWMs into a file
Args:
	filename
	PWM_list
'''
def print_PWMs(PWM_list,filename):
	with open(filename,"w") as f:
		sys.stdout = f

		#print ranked consensus PWMs
		for p in sorted(PWM_list, key= lambda item: item.log_KD_rel, reverse= False):
			print("", end = "\t")
			for i in range(0,len(p.PWM)//4):
				print((i+1), end = "\t\t")
			print("")
			for i in range(0,4):
				print(inv_nucl_dict[i], end = "\t")
				for j  in range(0,len(p.PWM)//4):
					print(p.PWM[i*len(p.PWM)//4+j], end = "\t")
				print("")
			print(p.E0, p.fg_log_lh, p.log_KD_rel, p.iterations)
			print("")
		sys.stdout = original_stdout

'''
BEGIN SCRIPT
'''
PWM_list = read_list_PWMs()
PWM_list = remove_duplicates(PWM_list)
PWM_list = set_rel_log_KDs(PWM_list)
print_PWMs(PWM_list,"ranked_KD.dat")




