#!/usr/bin/env python
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import logomaker


#matplotlib.rcParams.update({'font.size': 16})


color_scheme = { 'A' : '#00873E', \
	'C' : '#1175A8', \
	'G' : '#F6BE00', \
	'T' : '#B8293D', \
}

plt.rcParams['font.size'] = 20


f = open("PWMs_ranked_KD.dat","r")
ctr = 1

while True:
	# Get next PWM from file
	line1 = f.readline()	#skip first line
	line1 = f.readline()
	line2 = f.readline()
	line3 = f.readline()
	line4 = f.readline()
	line5 = f.readline()

	# if two lines are empty
	# end of file is reached
	if not (line1):
		break

	#extract words from lines
	val1 = line1.split()
	val2 = line2.split()
	val3 = line3.split()
	val4 = line4.split()


	#remove nucleotide labels
	val1.pop(0)
	val2.pop(0)
	val3.pop(0)
	val4.pop(0)

	#skip free line after each PWM
	line1 = f.readline()

	motif = pd.DataFrame(columns=['A','C','G','T'])

	for a,c,g,t in zip(val1,val2,val3,val4):
		tmp = np.array([a,c,g,t],dtype='float32')

		tmp += 0.01	#add pseudocount

		tmp /= 1.04	#renormalize

		row_ft = -sum(tmp * np.log2(tmp))

		motif.loc[len(motif)] = tmp * (2.0 - row_ft)

	fig,ax = plt.subplots()

	motif_logo = logomaker.Logo(motif,color_scheme=color_scheme,vpad=0,width=0.95,ax=ax)

	motif_logo.ax.set_xlim([-0.5,len(motif)-0.5])
	motif_logo.ax.set_ylim([0, 2])
	motif_logo.ax.set_xticks([])
	motif_logo.ax.set_yticks([0,2])
	motif_logo.ax.set_ylabel("Information content [bits]",rotation="vertical")
	motif_logo.style_spines(visible=False)

	plt.show()
	plt.savefig("motifs/motif"+str(ctr)+".pdf",bbox_inches = "tight")
	plt.close()

	ctr += 1

f.close()

