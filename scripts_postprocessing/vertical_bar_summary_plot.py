#!/usr/bin/env python
import os.path
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.image as image
from matplotlib.offsetbox import (OffsetImage, AnnotationBbox)
import logomaker
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition, mark_inset)


barwidth=0.15
bar_offset=0.2


color_scheme = { 'A' : '#00873E', \
	'C' : '#1175A8', \
	'G' : '#F6BE00', \
	'T' : '#B8293D', \
}

plt.rcParams['font.size'] = 28

'''
reads the PWM
'''
def read_motif(filename):
	motif = pd.read_csv(filename, sep='\t',header=None)

	motif = motif.dropna(axis=1)

	motif = motif.set_index(0)

	if len(motif) == 0:
		return None

	#add pseudocount
	motif = motif.astype('float64')
	motif = motif + 0.01

	#normalize
	motif = motif.divide(1.04, axis='rows')
	motif = motif.astype('float64')

	motif1 = pd.DataFrame()

	# convert the pwm to bits info for the logo
	for col in motif.columns:
		tmp = motif[col].to_numpy()

		row_ft = -sum(tmp * np.log2(tmp))

		motif1[col] = tmp * (2.0 - row_ft)

	motif = motif1.transpose()

	motif.columns = ['A','C','G','T']

	return motif


lowest_KD_rbps = pd.read_csv("lowest_KD/lowest_KD_motifs.tsv",sep="\t",low_memory=False,usecols=['RBP','log_rel_K_D','iterations'])
highest_KD_rbps = pd.read_csv("highest_KD/highest_KD_motifs.tsv",sep="\t",low_memory=False,usecols=['RBP','log_rel_K_D','iterations'])
consensus_KD_rbps = pd.read_csv("consensus_KD/consensus_motifs.tsv",sep="\t",low_memory=False,usecols=['RBP','log_rel_K_D','iterations'])

lowest_KD_rbps = lowest_KD_rbps[lowest_KD_rbps['iterations'] > 0]
highest_KD_rbps = highest_KD_rbps[highest_KD_rbps['iterations'] > 0]
consensus_KD_rbps = consensus_KD_rbps[consensus_KD_rbps['iterations'] > 0]

lowest_KD_rbps = pd.merge(lowest_KD_rbps,highest_KD_rbps,on="RBP",how='left')
lowest_KD_rbps = pd.merge(lowest_KD_rbps,consensus_KD_rbps,on="RBP",how='left')

lowest_KD_rbps.columns = ['RBP','log_rel_K_D_lowest','iterations_lowest','log_rel_K_D_highest','iterations_highest','log_rel_K_D_consensus','iterations_consensus']

lowest_KD_rbps = lowest_KD_rbps.sort_values('RBP',ascending=True)

N = len(lowest_KD_rbps)

bar_loc = np.arange(0,N)

bar_names = lowest_KD_rbps['RBP'].to_numpy()
bar_length_lowest = lowest_KD_rbps['log_rel_K_D_lowest'].to_numpy()
bar_length_highest = lowest_KD_rbps['log_rel_K_D_highest'].to_numpy()
bar_length_consensus = lowest_KD_rbps['log_rel_K_D_consensus'].to_numpy()


fig, (ax1,ax2,ax3,ax4) = plt.subplots(4,figsize=(30,38))

ax1.bar(bar_loc[:N//4]-bar_offset, bar_length_lowest[:N//4], width=barwidth, align='center', color='blue', label='random initialization, lowest $K_{D}$')
ax1.bar(bar_loc[:N//4], bar_length_highest[:N//4], width=barwidth, align='center', color='red', label='random initialization, highest $K_{D}$')
ax1.bar(bar_loc[:N//4]+bar_offset, bar_length_consensus[:N//4], width=barwidth, align='center', color='green', label='consensus initialization, lowest $K_{D}$')

ax1.set_xticks(bar_loc[:N//4],labels=bar_names[:N//4],rotation='vertical')

ax1.set_xlim([-0.5,N//4-0.5])
ax1.set_ylabel('$log_{10}$ $K_{D}^{rel}$',labelpad=0)
ax1.set_ylim([-4,7])

ax1.axhline(y=0, color='black',lw=0.8)

for x, r in zip(np.arange(0,N//4),bar_names[:N//4]):
	pwm = read_motif("lowest_KD/"+r+".dat")

	axins = ax1.inset_axes(((x+0.15)/(N//4),0.86,0.8/(N//4),0.13))

	motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

	motif_logo.ax.set_ylim([0, 2])
	motif_logo.style_spines(visible=False)
	motif_logo.ax.set_yticks([])
	motif_logo.ax.set_xticks([])


	if os.path.isfile("highest_KD/"+r+".dat"):
		pwm = read_motif("highest_KD/"+r+".dat")

		axins = ax1.inset_axes(((x+0.15)/(N//4),0.73,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])

	if os.path.isfile("consensus_KD/"+r+".dat"):
		pwm = read_motif("consensus_KD/"+r+".dat")

		axins = ax1.inset_axes(((x+0.15)/(N//4),0.6,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])


	if np.isnan(bar_length_consensus[x]) == True:
		ax1.text(x+bar_offset-0.07,0.07,'?')

ax1.legend(loc='lower left',labelspacing=0.0)

ax2.bar(bar_loc[N//4:2*(N//4)]-bar_offset, bar_length_lowest[N//4:2*(N//4)], width=barwidth, align='center', color='blue', label='random initialization, lowest $K_{D}$')
ax2.bar(bar_loc[N//4:2*(N//4)], bar_length_highest[N//4:2*(N//4)], width=barwidth, align='center', color='red', label='random initialization, highest $K_{D}$')
ax2.bar(bar_loc[N//4:2*(N//4)]+bar_offset, bar_length_consensus[N//4:2*(N//4)], width=barwidth, align='center', color='green', label='consensus initialization, lowest $K_{D}$')

ax2.set_xticks(bar_loc[N//4:2*(N//4)],labels=bar_names[N//4:2*(N//4)],rotation='vertical')

ax2.set_xlim([N//4-0.5,2*(N//4)-0.5])
ax2.set_ylabel('$log_{10}$ $K_{D}^{rel}$',labelpad=0)
ax2.set_ylim([-4,7])

ax2.axhline(y=0, color='black',lw=1.0)

for x, r in zip(np.arange(N//4,2*(N//4)),bar_names[N//4:2*(N//4)]):
	pwm = read_motif("lowest_KD/"+r+".dat")

	axins = ax2.inset_axes(((x+0.15-N//4)/(N//4),0.86,0.8/(N//4),0.13))

	motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

	motif_logo.ax.set_ylim([0, 2])
	motif_logo.style_spines(visible=False)
	motif_logo.ax.set_yticks([])
	motif_logo.ax.set_xticks([])


	if os.path.isfile("highest_KD/"+r+".dat"):
		pwm = read_motif("highest_KD/"+r+".dat")

		axins = ax2.inset_axes(((x+0.15-N//4)/(N//4),0.73,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])

	if os.path.isfile("consensus_KD/"+r+".dat"):
		pwm = read_motif("consensus_KD/"+r+".dat")

		axins = ax2.inset_axes(((x+0.15-N//4)/(N//4),0.6,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])


	if np.isnan(bar_length_consensus[x]) == True:
		ax2.text(x+bar_offset-0.07,0.07,'?')

ax3.bar(bar_loc[2*(N//4):3*(N//4)]-bar_offset, bar_length_lowest[2*(N//4):3*(N//4)], width=barwidth, align='center', color='blue')
ax3.bar(bar_loc[2*(N//4):3*(N//4)], bar_length_highest[2*(N//4):3*(N//4)], width=barwidth, align='center', color='red')
ax3.bar(bar_loc[2*(N//4):3*(N//4)]+bar_offset, bar_length_consensus[2*(N//4):3*(N//4)], width=barwidth, align='center', color='green')

ax3.set_xticks(bar_loc[2*(N//4):3*(N//4)],labels=bar_names[2*(N//4):3*(N//4)],rotation='vertical')

ax3.set_xlim([2*(N//4)-0.5,3*(N//4)-0.5])
ax3.set_ylabel('$log_{10}$ $K_{D}^{rel}$',labelpad=0)
ax3.set_ylim([-4,7])

ax3.axhline(y=0, color='black',lw=1.0)

for x, r in zip(np.arange(2*(N//4),3*(N//4)),bar_names[2*(N//4):3*(N//4)]):
	pwm = read_motif("lowest_KD/"+r+".dat")

	axins = ax3.inset_axes(((x+0.15-2*(N//4))/(N//4),0.86,0.8/(N//4),0.13))

	motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

	motif_logo.ax.set_ylim([0, 2])
	motif_logo.style_spines(visible=False)
	motif_logo.ax.set_yticks([])
	motif_logo.ax.set_xticks([])


	if os.path.isfile("highest_KD/"+r+".dat"):
		pwm = read_motif("highest_KD/"+r+".dat")

		axins = ax3.inset_axes(((x+0.15-2*(N//4))/(N//4),0.73,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])

	if os.path.isfile("consensus_KD/"+r+".dat"):
		pwm = read_motif("consensus_KD/"+r+".dat")

		axins = ax3.inset_axes(((x+0.15-2*(N//4))/(N//4),0.6,0.8/(N//4),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])


	if np.isnan(bar_length_consensus[x]) == True:
		ax3.text(x+bar_offset-0.07,0.05,'?')


ax4.bar(bar_loc[3*(N//4):]-bar_offset, bar_length_lowest[3*(N//4):], width=barwidth, align='center', color='blue')
ax4.bar(bar_loc[3*(N//4):], bar_length_highest[3*(N//4):], width=barwidth, align='center', color='red')
ax4.bar(bar_loc[3*(N//4):]+bar_offset, bar_length_consensus[3*(N//4):], width=barwidth, align='center', color='green')

ax4.set_xticks(bar_loc[3*(N//4):],labels=bar_names[3*(N//4):],rotation='vertical')

ax4.set_xlabel('RBP',labelpad=10)
ax4.set_xlim([3*(N//4)-0.5,N-0.5])
ax4.set_ylabel('$log_{10}$ $K_{D}^{rel}$',labelpad=0)
ax4.set_ylim([-4,7])

ax4.axhline(y=0, color='black',lw=1.0)

for x, r in zip(np.arange(3*(N//4),N),bar_names[3*(N//4):]):
	pwm = read_motif("lowest_KD/"+r+".dat")

	axins = ax4.inset_axes(((x+0.15-3*(N//4))/(N - 3*(N//4)),0.86,0.8/(N - 3*(N//4)),0.13))

	motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

	motif_logo.ax.set_ylim([0, 2])
	motif_logo.style_spines(visible=False)
	motif_logo.ax.set_yticks([])
	motif_logo.ax.set_xticks([])


	if os.path.isfile("highest_KD/"+r+".dat"):
		pwm = read_motif("highest_KD/"+r+".dat")

		axins = ax4.inset_axes(((x+0.15-3*(N//4))/(N - 3*(N//4)),0.73,0.8/(N - 3*(N//4)),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])


	if os.path.isfile("consensus_KD/"+r+".dat"):
		pwm = read_motif("consensus_KD/"+r+".dat")

		axins = ax4.inset_axes(((x+0.15-3*(N//4))/(N - 3*(N//4)),0.6,0.8/(N - 3*(N//4)),0.13))

		motif_logo = logomaker.Logo(pwm,color_scheme=color_scheme,vpad=0,width=0.95,ax=axins)

		motif_logo.ax.set_ylim([0, 2])
		motif_logo.style_spines(visible=False)
		motif_logo.ax.set_yticks([])
		motif_logo.ax.set_xticks([])


	if np.isnan(bar_length_consensus[x]) == True:
		ax4.text(x+bar_offset-0.07,0.07,'?')


plt.subplots_adjust(hspace=0.41)

plt.savefig('vertical_bar_plot.pdf', bbox_inches = "tight")
