#!/bin/bash

for RBP_name in A1CF HNRNPC RBM24 AKAP8L HNRNPCL1 PABPC3 RBM25 SUCLG1 APOBEC3C HNRNPD PABPN1L RBM3 SYNCRIP BOLL HNRNPDL RBM4 TAF15 HNRNPF PCBP1 RBM41 TARDBP CELF1 HNRNPH2 PCBP2 RBM45 TDRD10 CNOT4 HNRNPK PCBP4 RBM47 THUMPD1 HNRNPL RBM4B TIA1 CPEB1 IGF2BP1 RBM5 TRA2A CSDE1 IGF2BP2 RBM6 TRA2B DAZ3 IGF2BP3 RBMS2 TRNAU1AP DAZAP1 ILF2 PPP1R10 RBMS3 TROVE2 EIF3D PRR3 RC3H1 UNK EIF4G2 KHDRBS2 PTBP3 SAFB2 EIF4H KHDRBS3 PUF60 SF1 XRCC6 ELAVL4 KHSRP PUM1 SF3B6 XRN2 ESPR1 RALYL SFPQ YBX2 EWSR1 LIN28B SNRPB2 ZC3H10 EXOSC4 SRSF10 ZC3H18 FUBP1 RBFOX2 SRSF11 ZCRB1 FUBP3 MBNL1 RBFOX3 SRSF2 ZFP36 FUS MSI1 RBM11 SRSF4 ZFP36L1 NOVA1 RBM15B SRSF5 ZFP36L2 HNRNPA0 NSUN2 RBM20 SRSF8 ZNF326 HNRNPA1L2 NUPL2 RBM22 SRSF9 ZRANB2 HNRNPA2B1 RBM23 SNRPA
do
	cd ${RBP_name}

	if [[ $1 == "rank" ]]
	then
		cp ../rank_kmers_KD.py .

		conda run -n base python3 rank_kmers_KD.py ${RBP_name}

		rm rank_kmers_KD.py
	elif [[ $1 == "consensus" ]]
	then
		cp ../consensus_rank_kmers_KD.py .

		rm *.err

		cat consensus_${RBP_name}_5mer_*.dat > consensus_${RBP_name}_5mer.dat
		cat consensus_${RBP_name}_6mer_*.dat > consensus_${RBP_name}_6mer.dat

		conda run -n base python3 consensus_rank_kmers_KD.py consensus_${RBP_name}

		rm consensus_rank_kmers_KD.py
	else
		cp ../plot_motifs.py .

		rm -r motifs/

		mkdir motifs/

		conda run -n base python3 plot_motifs.py

		rm plot_motifs.py
	fi

	cd ../
done
