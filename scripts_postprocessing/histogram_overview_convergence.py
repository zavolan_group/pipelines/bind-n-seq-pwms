#!/usr/bin/python
'''
script plots histogram 
input needs form like
	python histogram.py
'''
from sys import argv
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


plt.rcParams['font.size'] = 28

spec_threshold = 0.0

'''
define class of PWMs with score
'''
class PWM_Score():
	def __init__(self, PWM, fg_log_lh, diff_log_lh, iterations, E0):
		self.PWM = PWM
		self.fg_log_lh = fg_log_lh
		self.diff_log_lh = diff_log_lh
		self.iterations = iterations
		self.E0 = E0



'''
START SCRIPT
'''
number_PWMs = {}
number_convergent_PWMs = {}
number_convergent_specific_PWMs = {}

for RBP in sorted(["A1CF","HNRNPC","RBM24","AKAP8L","HNRNPCL1","PABPC3","RBM25","SUCLG1","APOBEC3C","HNRNPD","PABPN1L","RBM3","SYNCRIP","BOLL","HNRNPDL","RBM4","TAF15","HNRNPF","PCBP1","RBM41","TARDBP","CELF1","HNRNPH2","PCBP2","RBM45","TDRD10","CNOT4","HNRNPK","PCBP4","RBM47","THUMPD1","HNRNPL","RBM4B","TIA1","CPEB1","IGF2BP1","RBM5","TRA2A","CSDE1","IGF2BP2","RBM6","TRA2B","DAZ3","IGF2BP3","RBMS2","TRNAU1AP","DAZAP1","ILF2","PPP1R10","RBMS3","TROVE2","EIF3D","PRR3","RC3H1","UNK","EIF4G2","KHDRBS2","PTBP3","SAFB2","EIF4H","KHDRBS3","PUF60","SF1","XRCC6","ELAVL4","KHSRP","PUM1","SF3B6","XRN2","ESPR1","RALYL","SFPQ","YBX2","EWSR1","LIN28B","SNRPB2","ZC3H10","EXOSC4","SRSF10","ZC3H18","FUBP1","RBFOX2","SRSF11","ZCRB1","FUBP3","MBNL1","RBFOX3","SRSF2","ZFP36","FUS","MSI1","RBM11","SRSF4","ZFP36L1","NOVA1","RBM15B","SRSF5","ZFP36L2","HNRNPA0","NSUN2","RBM20","SRSF8","ZNF326","HNRNPA1L2","NUPL2","RBM22","SRSF9","ZRANB2","HNRNPA2B1","RBM23","SNRPA"],key=str.lower):
	#fill in zeros
	number_PWMs[RBP] = 0
	number_convergent_PWMs[RBP] = 0
	number_convergent_specific_PWMs[RBP] = 0

	for fi in os.listdir(os.getcwd()+"/"+RBP):
		filename = RBP+"/"+os.fsdecode(fi)

		if ((not os.fsdecode(fi).startswith("ENCFF")) and filename.endswith("mer.dat") and os.fsdecode(fi).startswith(RBP)):
			f = open(filename,'r')

			while True:
				# Get next PWM from file
				line1 = f.readline()	#skip first line
				line1 = f.readline()
				line2 = f.readline()
				line3 = f.readline()
				line4 = f.readline()
				line5 = f.readline()

				# if two lines are empty
				# end of file is reached
				if not (line1):
					break

				#extract words from lines
				val1 = line1.split()
				val2 = line2.split()
				val3 = line3.split()
				val4 = line4.split()
				val5 = line5.split()

				#skip free line after each PWM
				line1 = f.readline()

				Lstart = float(val5[1])

				line1 = f.readline()	#skip first line
				line1 = f.readline()
				line2 = f.readline()
				line3 = f.readline()
				line4 = f.readline()
				line5 = f.readline()

				# if two lines are empty
				# end of file is reached
				if not (line1):
					break

				#extract words from lines
				val1 = line1.split()
				val2 = line2.split()
				val3 = line3.split()
				val4 = line4.split()
				val5 = line5.split()

				#skip free line after each PWM
				line1 = f.readline()

				Lw = len(val1) - 1
				Lstop = float(val5[1])
				E0 = float(val5[0])

				number_PWMs[RBP] += 1

				if (Lstart < Lstop):
					number_convergent_PWMs[RBP] += 1

					if E0 < spec_threshold:
						number_convergent_specific_PWMs[RBP] += 1

			f.close()


frequency_convergent_PWMs = {}
frequency_convergent_specific_PWMs = {}
for p in number_PWMs.keys():
	if number_PWMs[p] > 0.0:
		frequency_convergent_PWMs[p] = number_convergent_PWMs[p] / number_PWMs[p]
		frequency_convergent_specific_PWMs[p] = number_convergent_specific_PWMs[p] / number_PWMs[p]
	else:
		frequency_convergent_PWMs[p] = 0.0
		frequency_convergent_specific_PWMs[p] = 0.0


fig,(ax1,ax2,ax3) = plt.subplots(3,figsize=(30,32))

ax3.set_xlabel("RBP")
ax1.set_ylabel("fraction of runs")
ax2.set_ylabel("fraction of runs")
ax3.set_ylabel("fraction of runs")

RBP_names = [*frequency_convergent_PWMs.keys()]
RBPs_conv_freq = [*frequency_convergent_PWMs.values()]
RBPs_conv_spec_freq = [*frequency_convergent_specific_PWMs.values()]
N_RBPs = len(RBP_names)

ax1.set_xlim([-0.5,(N_RBPs//3)-0.5])
ax2.set_xlim([(N_RBPs//3)-0.5,2*(N_RBPs//3)-0.5])
ax3.set_xlim([2*(N_RBPs//3)-0.5,N_RBPs-0.5])

ax1.set_xticks(range(0,(N_RBPs//3)),RBP_names[:(N_RBPs//3)],rotation="vertical")
ax2.set_xticks(range((N_RBPs//3),2*(N_RBPs//3)),RBP_names[(N_RBPs//3):2*(N_RBPs//3)],rotation="vertical")
ax3.set_xticks(range(2*(N_RBPs//3),N_RBPs),RBP_names[2*(N_RBPs//3):],rotation="vertical")

ax1.set_ylim([0.0,1.0])
ax2.set_ylim([0.0,1.0])
ax3.set_ylim([0.0,1.0])
ax1.set_yticks([0.0,0.2,0.4,0.6,0.8,1.0])
ax2.set_yticks([0.0,0.2,0.4,0.6,0.8,1.0])
ax3.set_yticks([0.0,0.2,0.4,0.6,0.8,1.0])

ax1.bar(range(0,(N_RBPs//3)),RBPs_conv_freq[:(N_RBPs//3)],width=0.7,label="convergent",color="blue")
ax1.bar(range(0,(N_RBPs//3)),RBPs_conv_spec_freq[:(N_RBPs//3)],width=0.7,label="convergent and specific",color="red")

ax2.bar(range((N_RBPs//3),2*(N_RBPs//3)),RBPs_conv_freq[(N_RBPs//3):2*(N_RBPs//3)],width=0.7,label="convergent",color="blue")
ax2.bar(range((N_RBPs//3),2*(N_RBPs//3)),RBPs_conv_spec_freq[(N_RBPs//3):2*(N_RBPs//3)],width=0.7,label="convergent and specific",color="red")

ax3.bar(range(2*(N_RBPs//3),N_RBPs),RBPs_conv_freq[2*(N_RBPs//3):],width=0.7,label="convergent",color="blue")
ax3.bar(range(2*(N_RBPs//3),N_RBPs),RBPs_conv_spec_freq[2*(N_RBPs//3):],width=0.7,label="convergent and specific",color="red")

plt.subplots_adjust(hspace=0.4)

ax1.legend(loc='upper right',labelspacing=0.1)

plt.savefig("histogram_convergence.pdf",bbox_inches="tight")
