import Bio
import sys
import math
from Bio import SeqIO
import numpy as np
from scipy.optimize import fsolve

#declare input
background_files = []
foreground_files = []
b_kmer_len=0

#initialize input
if len(sys.argv) > 2:
	b_kmer_len = int(sys.argv[1])
	for ind in range(3,3+int(sys.argv[2])):
		background_files.append(sys.argv[ind])
	for ind in range(3+int(sys.argv[2]),len(sys.argv)):
		foreground_files.append(sys.argv[ind])
else:
	print("Insufficient input\n")

min_len=b_kmer_len



#save original output channel
original_stdout = sys.stdout




'''
define function
which calculates the background log frequency
of a given read S
'''
def construct_frequency(S,background_log_freq_dict):
	ret = 0.0
	
	for kmer_start in range(0,len(S)-b_kmer_len+1):
		kmer = S[kmer_start:kmer_start+b_kmer_len]
		if kmer in background_log_freq_dict:
			ret += background_log_freq_dict[kmer]
		else:
			return 0.0
	
	if ret < -1000000.0:
		return 0.0

	return np.exp(ret,dtype='float64')



'''
define method
that constructs background sequence counts dictionary
from background pool
'''
def background_sequence_dict(files):
	background_dict = {}

	for data_file in files:
		for read in SeqIO.parse(data_file+".fastq","fastq"):
			s = read.seq

			if 'N' not in s and len(s) >= min_len:
				if s not in background_dict:
					background_dict[s] = 1
				else:
					background_dict[s] += 1

	return background_dict



'''
define method
that constructs background kmer counts dictionary
from background sequence dict
'''
def background_kmer_dict(background_dict):
	kmer_dict = {}

	for s,m in background_dict.items():
			for kmer_start in range(0,len(s)-b_kmer_len+1):
				kmer = s[kmer_start:kmer_start+b_kmer_len]

				if kmer not in kmer_dict:
					kmer_dict[kmer] = m
				else:
					kmer_dict[kmer] += m

	return kmer_dict



polynomial = {}
'''
callable for normalization polynomial
'''
def poly(N):
	powers = np.array(list(polynomial.keys()),dtype='int32')
	coefficients = np.array(list(polynomial.values()),dtype='float64')

	ret = sum(coefficients*np.power(N,powers)) - 1.0

	return ret



'''
define method
that normalizes the kmer frequencies
such that sum_S f_S = 1 for S in background is enforced

involves solving nonlinear equation
polynomial needs to be defined globally
since python does not allow function parameters to be set on the fly :/
'''
def normalize_background_kmer_freq_dict(background_sequence_dict,background_kmer_dict):
	global polynomial

	norm = sum(background_kmer_dict.values())
	norm_log_counts = {k : np.log(v/norm) for k,v in background_kmer_dict.items()}

	for s in background_sequence_dict.keys():
		tmp_bgr = construct_frequency(s,norm_log_counts)

		N_kmers = len(s) - b_kmer_len + 1

		if tmp_bgr > 0.0 and N_kmers > 0:
			if N_kmers not in polynomial:
				polynomial[N_kmers] = tmp_bgr
			else:
				polynomial[N_kmers] += tmp_bgr

	#rescale polynomial, necessary!
	maxpow = max(polynomial.keys())
	maxpow_coeff = polynomial[maxpow]

	fact = np.power(maxpow_coeff,-1.0/maxpow)

	polynomial = {p : c*np.power(fact,p) for p,c in polynomial.items()}

	norm = fsolve(poly, 0.5, maxfev=2000)[0]
	norm *= fact

	background_kmer_log_freq = {k : (v + np.log(norm)) for k,v in norm_log_counts.items()}

	return background_kmer_log_freq



'''
print background likelihood of kmer
'''
def print_kmer_dict(kmer_dict):
	with open("background_log_kmer_frequencies_"+str(b_kmer_len)+"mer.dat","w") as f:
		sys.stdout = f
		for k,f in kmer_dict.items():
			print(k,f)
		sys.stdout = original_stdout


'''
create foreground sequence dictionaries, print in data format for RBAPBindnSeq
'''
def prepare_foreground_data(foreground_files,background_kmer_log_freq):
	for data_file in foreground_files:
		foreground_dict = {}

		#create foreground dictionary
		for seq_record in SeqIO.parse(data_file+".fastq","fastq"):
			s = seq_record.seq

			if 'N' not in s and len(s) >= min_len:
				if s in foreground_dict:
					foreground_dict[s] += 1
				else:
					foreground_dict[s] = 1



		#print read files with background frequencies
		with open(data_file+"_"+str(b_kmer_len)+"mer.dat","w") as f:
			sys.stdout = f
			for s in foreground_dict:
				tmp_bgr = construct_frequency(s,background_kmer_log_freq)
				if tmp_bgr > 0.0 and len(s) >= b_kmer_len:
					print(len(s),foreground_dict[s],tmp_bgr,s)

			sys.stdout = original_stdout


back_seq_dict = background_sequence_dict(background_files)
back_kmer_dict = background_kmer_dict(back_seq_dict)

norm_background_dict = normalize_background_kmer_freq_dict(back_seq_dict,back_kmer_dict)

print_kmer_dict(norm_background_dict)
prepare_foreground_data(foreground_files,norm_background_dict)
