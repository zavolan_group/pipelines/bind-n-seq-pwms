-----------------------------------------------
Niels Schlusser, April, 22th 2024
-----------------------------------------------

This directory contains the python code and bash scripts to turn *.fastq.gz files into the desired format for our EM optimization of binding sites. To this end, an the background frequencies need to be constructed from a Markov model of highest possible degree.

The code was ran on the following system configuration
	- Ubuntu 20.04.1
	- python 3.8.10
	- Biopython 1.79
	- Bash 5.0.7(1)
	- cutadapt 3.4
	
The code is organized in two parts: The actual Markov model is run in a python script ("construct_background.py", not parallelized), whereas the overarching tasks, including the execution of the python script itself, are organized in the bash script "execute.sh".

Parameters need to be specified in the bash script "execute.sh", only:
	- the degree of the Markov model. It should be as long as possible. In practice, a length of 14 has proven feasible with a decent RAM consumption (~16GB).
	- the number of background files
	- the (ENCODE-format) filenames (without *.fastq.gz extension) for foreground and background
	
For downloading and adapter trimming, execute the script with <./execute.sh dlct>. This will yield *.fastq files with trimmed adapters. After this, run <./execute.sh> for the file preparation using the Markov model.

