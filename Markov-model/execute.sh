#!/bin/bash

Lw=4			#kmer-length
nbr_bfiles=1	#number of background files

#input file names
background_files=("ENCFF295WRQ")
foreground_files=("ENCFF278GIB" "ENCFF374UTI" "ENCFF400JXE" "ENCFF815CIK" "ENCFF897ONM")



####################
####START SCRIPT####
####################

#download and cut adapter mode
if [[ $1 == "dlct" ]]
then
	#download files
	for filename in ${background_files[@]}
	do
		curl -O -L https://www.encodeproject.org/files/${filename}/@@download/${filename}.fastq.gz
	done

	for filename in ${foreground_files[@]}
	do
		curl -O -L https://www.encodeproject.org/files/${filename}/@@download/${filename}.fastq.gz
	done


	#turn into fastq and cut adapter
	module purge
	ml cutadapt

	for filename in ${background_files[@]}
	do
		cutadapt -j 4 -a TGGAATTCTCGGGTGTCAAGG -o ${filename}.fastq ${filename}.fastq.gz
	done

	for filename in ${foreground_files[@]}
	do
		cutadapt -j 4 -a TGGAATTCTCGGGTGTCAAGG -o ${filename}.fastq ${filename}.fastq.gz
	done
else
	#calculates background frequencies based on Markov model
	module purge
	ml Biopython
	python3 construct_background.py ${Lw} ${nbr_bfiles} $(echo "${background_files[*]}") $(echo "${foreground_files[*]}")

	for filename in ${foreground_files[@]}
	do
		awk 'BEGIN{max=0}{if($1>max) max=$1}END{print "'${filename}'",max}' ${filename}.fastq.gz
	done

	rm background_frequencies_${Lw}mer.dat
fi
