#!/bin/bash

#SBATCH --job-name=construct_kMM   #This is the name of your job
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1   #This is the number of cores reserved
#SBATCH --mem=16G    #This is the memory reserved.
#SBATCH --time=1-00:00:00        #This is the time that your task will run
#SBATCH --qos=1day      #You will run in this queue
#SBATCH --output=output_%j.dat    #Output file
#SBATCH --error=error_%j.err     #Error file


#add your command lines below
bash execute.sh
