#ifndef INOUT_H
#define INOUT_H



/*
 * method reads in the sequences
 * and their lengths
 * 
 * internally encodes
 * 	- "A" -> 0
 * 	- "C" -> 1
 * 	- "G" -> 2
 * 	- "T" -> 3
 */
void read_sequences()
{
	ifstream myFile;
	int tmp_int,offset;
	string tmp_string;
	double tmp_dbl;
	
	offset = 0;
	for(int f=0;f<NBR_FILES;f++)
	{
		myFile.open(input_files_array[f].filename);	//for source file
		
		for(int i=offset;i<(offset+input_files_array[f].nbr_lines);i++)
		{
			tmp_int = 0;
			myFile >> tmp_int;
			sequence_array[i].length = tmp_int;
			
			tmp_int = 0;
			myFile >> tmp_int;
			sequence_array[i].amt = tmp_int;
			
			tmp_dbl = 0.0;
			myFile >> tmp_dbl;
			sequence_array[i].freq = tmp_dbl;
			
			myFile >> tmp_string;
			const char *tmp_char = tmp_string.c_str();
			
			for(int j=0;j<sequence_array[i].length;j++)
			{
				switch(tmp_char[j])
				{
					case 'A':
						sequence_array[i].seq[j] = 0;
						break;
					case 'C':
						sequence_array[i].seq[j] = 1;
						break;
					case 'G':
						sequence_array[i].seq[j] = 2;
						break;
					case 'T':
						sequence_array[i].seq[j] = 3;
						break;
					default:
						printf("Unknown base in file number %d, sequence number %d at position %d",f,(i-offset),j);
				}
			}
		}
		
		myFile.close();
		
		offset += input_files_array[f].nbr_lines;
	}
}



/*
 * method reads masked sequences
 * 
 * internally encodes
 * 	- "A" -> 0
 * 	- "C" -> 1
 * 	- "G" -> 2
 * 	- "T" -> 3
 */
void read_mask(const char* fName)
{
	ifstream myFile;
	string tmp_string;
	int ind,tmp;
	
	myFile.open(fName);	// for source file
	
	for(int i=0;i<NBR_MASK;i++)
	{
		myFile >> tmp_string;
		const char *tmp_char = tmp_string.c_str();
		
		ind = 0;
		for(int j=0;j<RBP_BIND_LEN;j++)
		{
			switch(tmp_char[j])
			{
				case 'A':
					tmp = 0;
					break;
				case 'C':
					tmp = 1;
					break;
				case 'G':
					tmp = 2;
					break;
				case 'T':
					tmp = 3;
					break;
				default:
					printf("Unknown base in sequence number %d at position %d\n",i,j);
			}
			
			ind += tmp*pow(4,j);
		}
		
		masked_array[ind] = true;
	}
	
	myFile.close();
}



/*
 * prints the sequence information
 */
void print_seq(sequence *sequence_array)
{
	for(int i=0;i<tot_nbr_sequences;i++)
	{
		printf("%d\t%d\t%f\t",sequence_array[i].length,sequence_array[i].amt,sequence_array[i].freq);
		
		for(int j=0;j<sequence_array[i].length;j++)	printf("%d\t",sequence_array[i].seq[j]);
		printf("\n");
	}
}



/*
 * prints the status of the update
 * gives actual PWM entries
 */
void print_state(state *curr_state)
{
	printf("\t");
	for(int j=1;j<=RBP_BIND_LEN;j++)
		printf("%d\t\t",j);
	printf("\n");
	
	for(int i=0;i<4;i++)
	{
		switch(i)
		{
			case 0:
				printf("A\t");
				break;
			case 1:
				printf("C\t");
				break;
			case 2:
				printf("G\t");
				break;
			case 3:
				printf("T\t");
		}
		for(int j=0;j<RBP_BIND_LEN;j++)
			printf("%f\t",(*curr_state).wai[RBP_BIND_LEN*i+j]);
		printf("\n");
	}
	printf("%f\t",(*curr_state).E0);
}



/*
 * auxiliary method
 * converts the a struct state 
 * into a gsl vector
 * vector's first entry is e^E_0
 * following 4 * RBP_BIND_LEN are epsilons
 * leading to the PWM elements via exponentiation
 */
void state_to_vec(state *curr_state,gsl_vector *vec)
{
	int i,j;
	
	gsl_vector_set(vec,0,(*curr_state).E0);
	
	for(i=0;i<4;i++)
		for(j=0;j<RBP_BIND_LEN;j++)
			gsl_vector_set(vec,(RBP_BIND_LEN*i+j+2),(*curr_state).wai[RBP_BIND_LEN*i+j]);
}



/*
 * auxiliary method
 * converts the a gsl vector
 * into a struct state
 * vector's first entry is e^E_0
 * following 4 * RBP_BIND_LEN are \epsilons 
 * leading to the PWM elements via exponentiation
 */
void vec_to_state(gsl_vector *vec,state *curr_state)
{
	int i,j;
	
	(*curr_state).E0 = gsl_vector_get(vec,0);
	
	for(i=0;i<4;i++)
		for(j=0;j<RBP_BIND_LEN;j++)
			(*curr_state).wai[RBP_BIND_LEN*i+j] = gsl_vector_get(vec,(RBP_BIND_LEN*i+j+1));
}


#endif
