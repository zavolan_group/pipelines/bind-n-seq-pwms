#ifndef PARAMS_H
#define PARAMS_H


/*define length of the RBP binding site, has to be <= minimal sequence length*/
#define THREADS			4
#define RBP_BIND_LEN	5
#define MAX_SEQ_LEN	20
#define NBR_FILES			1
#define NBR_MASK			0

double 	precision	= 1.0e-6;	//quadratic accuracy

const char* mask_file 	= "mask_seq.dat";
const static struct input_file input_files_array [NBR_FILES] = 
{
	{"ENCFF004OKN.dat",10360176},
	{"ENCFF261ZHZ.dat",7994226},
	{"ENCFF281CNE.dat",8463881},
	{"ENCFF674RCB.dat",10001930},
	{"ENCFF732XFP.dat",6552943}
};

#endif
