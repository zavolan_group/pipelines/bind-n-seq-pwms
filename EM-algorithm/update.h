#ifndef UPDATE_H
#define UPDATE_H


/*
 * auxiliary method checks 
 * if sequence s
 * starting at ind
 * with length RBP_BIND_LEN
 * is masked
 * 
 * input: struct state
 */
bool is_masked(int s,int ind)
{
	int tmp=0;
	
	for(int i=0;i<RBP_BIND_LEN;i++)
		tmp += sequence_array[s].seq[ind+i]*pow(4,i);
	
	return masked_array[tmp];
}



/*
 * auxiliary method copies
 * original state into copy
 */
void copy_state(state *original,state *copy)
{
	(*copy).E0 = (*original).E0;
	
	for(int i=0;i<4*RBP_BIND_LEN;i++)
		(*copy).wai[i] = (*original).wai[i];
}



/*
 * auxiliary method 
 * fills state with zeros
 */
void zero_state(state *st)
{
	(*st).E0 = 0.0;
	
	for(int i=0;i<4*RBP_BIND_LEN;i++)
		(*st).wai[i] = 0.0;
}



/*
 * auxiliary method calculates
 * the metric difference between 
 * two states
 * using 
 * 	||s1 - s2 || \equiv 
 * 		1/components \sum_{ij} (s1_{ij} - s2_{ij})^2 
 */
double metric_difference(state *s1,state *s2)
{
	double ret;
	
	ret = pow(((*s1).E0 - (*s2).E0),2);
	
	for(int i=0;i<4*RBP_BIND_LEN;i++)
		ret += pow(((*s1).wai[i] - (*s2).wai[i]),2);
	
	ret /= (4*RBP_BIND_LEN + 1);
	
	return ret;
}



/*
 * method initializes matrix 
 * randomly
 */
void initialize_matrix(state *curr_state)
{
	(*curr_state).E0 =	-1.0*rand()/RAND_MAX;	//random initialization of state

	for(int i=0;i<4*RBP_BIND_LEN;i++)
	{
		(*curr_state).wai[i] = exp(-8.0*rand()/RAND_MAX);
	}
}



/*
 * method initializes matrix 
 * at given input sequence
 */
void initialize_matrix(state *curr_state,char* input_matrix)
{
	(*curr_state).E0 =	-1.0*rand()/RAND_MAX;	//random initialization of state
	
	for(int i=0;i<4*RBP_BIND_LEN;i++)
		(*curr_state).wai[i] = 0.001;
	
	for(int i=0;i<RBP_BIND_LEN;i++)
	{
		switch(input_matrix[i])
		{
			case 'A':
				(*curr_state).wai[RBP_BIND_LEN*0+i] = 0.997;
				break;
			case 'C':
				(*curr_state).wai[RBP_BIND_LEN*1+i] = 0.997;
				break;
			case 'G':
				(*curr_state).wai[RBP_BIND_LEN*2+i] = 0.997;
				break;
			case 'T':
				(*curr_state).wai[RBP_BIND_LEN*3+i] = 0.997;
				break;
			case 'N':
				for(int j=0;j<4;j++)	
					(*curr_state).wai[RBP_BIND_LEN*j+i] = 0.25;
				break;
			case 'R':
				for(int j=0;j<4;j++)	
					(*curr_state).wai[RBP_BIND_LEN*j+i] = exp(-8.0*rand()/RAND_MAX);;
				break;
			default:
				printf("Error: Input sequence length does not match parameter!\n");
		}
	}
}



/*
 * auxiliary method calculates e^E(s)
 * of sequence s
 * 
 * input: double PWM and sequence index
 */
double exp_ES(double *pWai,int s)
{
	double tmp,eES;
	
	eES = 0.0;
	for(int i=0;i<sequence_array[s].length-RBP_BIND_LEN+1;i++)
	{
		tmp = 1.0;
		for(int j=0;j<RBP_BIND_LEN;j++)
			tmp *= pWai[RBP_BIND_LEN*sequence_array[s].seq[i+j]+j];
		
#if NBR_MASK > 0
		if(is_masked(s,i) == true)
			tmp = 0.0;
#endif
		
		eES += tmp;
	}
	
	return eES;
}



/*
 * auxiliary method calculates e^E(s)
 * of sequence s
 * 
 * input: struct state
 */
double exp_ES(state *curr_state,int s)
{
	double tmp,eES;
	
	eES = 0.0;
	for(int i=0;i<sequence_array[s].length-RBP_BIND_LEN+1;i++)
	{
		tmp = 1.0;
		for(int j=0;j<RBP_BIND_LEN;j++)
			tmp *= (*curr_state).wai[RBP_BIND_LEN*sequence_array[s].seq[i+j]+j];
		
#if NBR_MASK > 0
		if(is_masked(s,i) == true)
			tmp = 0.0;
#endif
		
		eES += tmp;
	}
	
	return eES;
}




/*
 * method calculates M I N U S the log-likelihood
 * 
 * E0 is current state energy
 * sequence information is available globally
 * 
 * input: E0 as double, 
 * params contains PWM as void
 */
double log_likelihood(double E0,void *params)
{
	double ret,norm1;
	int norm2;
	
	double 	pE0		= exp(E0);
	double *pWai	= ((struct state *)params) -> wai;
	
	ret 		= 0.0;
	norm1 	= 0.0;
	norm2 	= 0;
	
	#pragma omp parallel num_threads(THREADS) shared(ret,norm1,norm2,pE0,pWai,sequence_array)
	{
	double loc_ret			= 0.0;
	double loc_norm1	= 0.0;
	int loc_norm2			= 0.0;
	
	#pragma omp for
	for(int s=0;s<tot_nbr_sequences;s++)
	{
		int tmpA 		= sequence_array[s].amt;
		double tmp 	= sequence_array[s].freq * (exp_ES(pWai,s) + pE0*(sequence_array[s].length-RBP_BIND_LEN+1));
		
		loc_ret 			+= tmpA*log(tmp);
		loc_norm1 	+= tmp;
		loc_norm2 	+= tmpA;
	}
	
	#pragma omp critical
	{
	ret 		+= loc_ret;
	norm1 	+= loc_norm1;
	norm2 	+= loc_norm2;
	}
	}
	
	ret -= norm2*log(norm1);
	
	return -ret;
}



/*
 * method calculates P L U S the log-likelihood
 * 
 * vec contains the current state
 * sequence information is available globally
 * 
 * input: struct state
 */
double log_likelihood(state *curr_state)
{
	double ret,norm1;
	int norm2;
	
	double 	pE0	= exp((*curr_state).E0);
	
	ret 		= 0.0;
	norm1 	= 0.0;
	norm2 	= 0;
	
	#pragma omp parallel num_threads(THREADS) shared(ret,norm1,norm2,pE0,curr_state,sequence_array)
	{
	double loc_ret			= 0.0;
	double loc_norm1	= 0.0;
	int loc_norm2			= 0.0;
	
	#pragma omp for
	for(int s=0;s<tot_nbr_sequences;s++)
	{
		int tmpA 		= sequence_array[s].amt;
		double tmp 	= sequence_array[s].freq * (exp_ES(curr_state,s) + pE0*(sequence_array[s].length-RBP_BIND_LEN+1));
		
		loc_ret 			+= tmpA*log(tmp);
		loc_norm1 	+= tmp;
		loc_norm2 	+= tmpA;
	}
	
	#pragma omp critical
	{
	ret 		+= loc_ret;
	norm1 	+= loc_norm1;
	norm2 	+= loc_norm2;
	}
	}
	
	ret -= norm2*log(norm1);
	
	return ret;
}




/*
 * method enforces the normalization 
 * of each column in the PWM to 1
 * 
 * input: gsl_vector
 */
void renormalize(gsl_vector *vec)
{
	double tmp1,tmp2;
	
	for(int i=0;i<RBP_BIND_LEN;i++)
	{
		tmp1 = 0.0;
		for(int j=0;j<4;j++)
			tmp1 += gsl_vector_get(vec,RBP_BIND_LEN*j+i+1);
		
		tmp1 = log(tmp1);
		for(int j=0;j<4;j++)
		{
			tmp2 = gsl_vector_get(vec,RBP_BIND_LEN*j+i+1);
			tmp2 /= tmp1;
			gsl_vector_set(vec,RBP_BIND_LEN*j+i+1,tmp2);
		}
	}
}




/*
 * method enforces the normalization 
 * of each column in the PWM to 1
 * 
 * input: struct state
 */
void renormalize(state *curr_state)
{
	double tmp;
	
	for(int i=0;i<RBP_BIND_LEN;i++)
	{
		tmp = 0.0;
		for(int j=0;j<4;j++)
			tmp += (*curr_state).wai[RBP_BIND_LEN*j+i];
			
		for(int j=0;j<4;j++)
			(*curr_state).wai[RBP_BIND_LEN*j+i] /= tmp;
	}
}




/*
 * expectation step of the EM algorithm
 * maximizes the log-likelihood 
 * with repect to the baseline E_0
 * 
 * uses 1d gsl minimizer
 * 	(and minimizes minus the log-likelihood)
 * 
 * returns the log-likelihood
 */
double E_step(state *curr_state)
{
	int status,iter=0;
	double m=(*curr_state).E0;
	double a=-100.0,b=0.0;
	const gsl_min_fminimizer_type *T;
	gsl_min_fminimizer *smin;
	gsl_function F;
	
	F.function = &log_likelihood;
	F.params = curr_state;
	
	T = gsl_min_fminimizer_brent;
	smin = gsl_min_fminimizer_alloc(T);
	gsl_min_fminimizer_set(smin,&F,m,a,b);
	
	
	do
	{
		iter++;
		status = gsl_min_fminimizer_iterate(smin);
		
		m = gsl_min_fminimizer_x_minimum(smin);
		a = gsl_min_fminimizer_x_lower(smin);
		b = gsl_min_fminimizer_x_upper(smin);
		
		status = gsl_min_test_interval(a,b,0.001,0.0);
	}
	while(status == GSL_CONTINUE && iter < 200);
   
	(*curr_state).E0 = m;
	
	gsl_min_fminimizer_free(smin);
	
	return status;
}



/*
 * maximization step 
 * 
 */
void M_step(state *curr_state)
{
	double eE0;
	state duplicate;	//duplicate state
	
	//allocations
	duplicate.wai = new double[4*RBP_BIND_LEN];
	
	zero_state(&duplicate);
	duplicate.E0 = (*curr_state).E0;
	
	eE0	= exp((*curr_state).E0);
	
	#pragma omp parallel num_threads(THREADS) shared(duplicate,curr_state,eE0,sequence_array)
	{
	state local_duplicate;
	local_duplicate.wai = new double[4*RBP_BIND_LEN];
	zero_state(&local_duplicate);
	local_duplicate.E0 = (*curr_state).E0;
	
	#pragma omp for
	for(int s=0;s<tot_nbr_sequences;s++)
	{
		double *eEs,tmp;
		int tmpL 		= sequence_array[s].length-RBP_BIND_LEN+1;
		int tmpA 		= sequence_array[s].amt;
		double tmpF	= sequence_array[s].freq;
		
		eEs		= new double[tmpL];
		
		tmp		= 0.0;
		for(int i=0;i<tmpL;i++)
		{
			eEs[i] = 1.0;
			for(int j=0;j<RBP_BIND_LEN;j++)
				eEs[i] *= (*curr_state).wai[RBP_BIND_LEN*sequence_array[s].seq[i+j]+j];
			
			#if NBR_MASK > 0
			if(is_masked(s,i) == true)
				eEs[i] = 0.0;
			#endif
			
			tmp += eEs[i];
		}
		
		tmp += tmpL*eE0;
		
		for(int i=0;i<tmpL;i++)
			for(int j=0;j<RBP_BIND_LEN;j++)
				local_duplicate.wai[RBP_BIND_LEN*sequence_array[s].seq[i+j]+j] += 
					tmpA * eEs[i] / tmp;
		
		delete[] eEs;
	}
	
	#pragma omp critical
	{
	for(int i=0;i<4*RBP_BIND_LEN;i++)
		duplicate.wai[i] += local_duplicate.wai[i];
	}
	}
	
	copy_state(&duplicate,curr_state);
	
	delete[] duplicate.wai;
}


#endif
